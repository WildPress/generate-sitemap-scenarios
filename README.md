# Description
Generate a basic list of scenarios for BackstopJS using a provided sitemap.xml

# Usage
generate_sitemap_scenarios --sitemap="https://www.example.com/sitemap.xml"
