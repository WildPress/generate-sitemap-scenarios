package main

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
)

type SitemapIndex struct {
	XMLName     xml.Name  `xml:"sitemapindex"`
	SitemapList []Sitemap `xml:"sitemap"`
}

type Sitemap struct {
	XMLName      xml.Name `xml:"sitemap"`
	Location     string   `xml:"loc"`
	LastModified string   `xml:"lastmod"`
}

type URLSet struct {
	XMLName xml.Name `xml:"urlset"`
	URLList []Url    `xml:"url"`
}

type Url struct {
	XMLName      xml.Name `xml:"url"`
	Location     string   `xml:"loc"`
	LastModified string   `xml:"lastmod"`
}

type Scenario struct {
	Label string `json:"label"`
	URL   string `json:"url"`
}

func main() {

	// Setup the input flags
	sitemapPtr := flag.String("sitemap", "https://wordpack.co/sitemap.xml", "The sitemap's full URL")
	flag.Parse()

	sitemap := fetchSitemap(*sitemapPtr)

	var sitemaps = unmarshalSitemapIndex(sitemap)
	var scenarios = unmarshalSitemap(sitemap)

	for _, sitemap := range sitemaps {
		s := fetchSitemap(sitemap)
		u := unmarshalSitemap(s)

		scenarios = append(scenarios, u...)
	}

	marshalJsonScenarios(scenarios)
}

func fetchSitemap(sitemap string) []byte {
	// Validate the sitemap is a fully qualified URL
	url, err := url.ParseRequestURI(sitemap)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Fetching sitemap:", url.String())

	// Fetch the sitemap
	res, err := http.Get(url.String())
	if err != nil {
		log.Fatal(err)
	}

	// Read the request body
	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	return body
}

// Unmarshal the URL set
func unmarshalSitemap(sitemap []byte) []Scenario {
	var scenarios []Scenario
	var urlSet URLSet
	xml.Unmarshal(sitemap, &urlSet)

	for _, url := range urlSet.URLList {
		var scenario Scenario

		scenario.Label = url.Location
		scenario.URL = url.Location
		scenarios = append(scenarios, scenario)
	}

	return scenarios
}

// Unmarshal the sitemap index
func unmarshalSitemapIndex(sitemap []byte) []string {
	var urls []string
	var sitemapIndex SitemapIndex
	xml.Unmarshal(sitemap, &sitemapIndex)

	for _, url := range sitemapIndex.SitemapList {
		urls = append(urls, url.Location)
	}

	return urls
}

func marshalJsonScenarios(scenarios []Scenario) {
	json, err := json.Marshal(scenarios)
	if err != nil {
		log.Fatal(err)
	}

	os.Stdout.Write(json)
}
